const mongoose = require('mongoose');

// Definición del esquema de cliente
const clienteSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  cretedAt: {
    type: Date,
    default: Date.now
  },
  notes: {
    type: String
  },
  coordinates: {
    latitude: {
      type: Number,
      required: true
    },
    longitude: {
      type: Number,
      required: true
    }
  },
});

// Creación del modelo Cliente
const Cliente = mongoose.model('Client', clienteSchema);

module.exports = Cliente;

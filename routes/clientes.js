const Cliente = require('../models/clientes');
module.exports = (app, next) => { 
  app.get('/cliente', (req, resp) => { 
    const query = req.query?req.query:{};
    Cliente.find(query)
    .then(user => {
      resp.json(user);
    },(error)=>{
      resp.status(404).send(error);
    }).catch(next);
  });
  app.post('/cliente', (req, resp) => {
    Cliente.create(req.body)
    .then(user => {
      resp.json(user);
    },(error)=>{
      resp.status(404).send(error);
    }).catch(next);
  });
  app.put('/cliente/:id', (req, resp) => { 
    const body = req.body;
    const email = req.params.id;
    Cliente.updateOne({correo: email}, {$set: body})
    .then(user => {
      resp.json(user);
    },(error)=>{
      resp.status(404).send(error);
  }).catch(next)
  });
  app.delete('/cliente/:id', (req, resp) => { 
    const email = req.params.id;
    Cliente.deleteOne({email})
    .then(user => {
      resp.json(user);
    },(error)=>{
      resp.status(404).send(error);
  }).catch(next);
  });




  return next();
};